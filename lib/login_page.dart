import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final FirebaseAnalytics analytics = FirebaseAnalytics();

  String photo =
      "https://i2.wp.com/marvin.com.mx/wp-content/uploads/2019/07/becky-g-july-31-2018-billboard-1548.jpg";

  String name = " O ";
  String email = " O ";
  bool isSingIn = false;

  Future<void> _loginSuccess() async {
    await analytics.logLogin(loginMethod: "Google");
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Image.network(
                photo,
                width: 250,
                height: 250,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              child: Text(name),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              child: Text(email),
            ),
            SizedBox(
              height: 20,
            ),
            !isSingIn
                ? Container(
                    child: GoogleSignInButton(
                      borderRadius: 20.0,
                      text: 'Continuar con Google',
                      onPressed: () async {
                        UserCredential user = await signInWithGoogle();
                        User usuario = user.user;
                        setState(() {
                          photo = usuario.photoURL;
                          name = usuario.displayName;
                          email = usuario.email;
                          isSingIn = true;
                        });
                      },
                    ),
                  )
                : Container(
                    child: FlatButton(
                      onPressed: () {
                        print("SALIR");
                        logout();
                      },
                      child: Text("SALIR"),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Future<UserCredential> signInWithGoogle() async {
    try {
      // Trigger the authentication flow
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

      // Obtain the auth details from the request
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      // Create a new credential
      final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      // Once signed in, return the UserCredential

      UserCredential usuario =
          await FirebaseAuth.instance.signInWithCredential(credential);

      await analytics.logLogin(loginMethod: "Google");

      return usuario;
    } catch (e) {
      print("ERROR");
      print(e);
    }
  }

  void logout() async {
    try {
      await GoogleSignIn().signOut();
      setState(() {
        photo =
            "https://i2.wp.com/marvin.com.mx/wp-content/uploads/2019/07/becky-g-july-31-2018-billboard-1548.jpg";
        name = "O";
        email = "O";
        isSingIn = false;
      });

      await analytics.logEvent(
        name: "LogOut",
      );
    } catch (e) {
      print(e);
    }
  }
}
